# GOVIS Āhau 23 : distributed, replicating tribal archives

## Ideas

- read the topic of the conference
  - https://www.govis.org.nz/conference-2023/
  - Digital Resilience
- name what I want people to learn
- narratives for social change
- animate replication
- intro ahau
  - what is it?
  - why/ how does it exist?
  - p2p database?


- replicating databases
  1. sovereignty
  2. resilience
  3. plularistic data, data that's BETWEEN


- the context of the people
  - stuck in a local maxima thinking about "what cloud looks like"
  - transcendent resilience
    - what does resilience look like beyond aws/google/azure?
    - what happend in a cyclone
    - decolonise the conception of what a database looks like
    - your siloing set you up in opposition to te tiriti
- bring the audrey tang vibe
    - I'm from another place and I'm gonna talk about who I am and how data works
    - I'm visiting from the moon

## Slides

- A talk about data resilience
  - TODO: pull the title of the talk in
  - I work on Ahau, which is a tribal archiving tool (family trees, stories, document, video)
  - I want to share a story about the decisions we made about data, because there's a chance it's relevant to how government thinks about and works with data

### PART 1: RESILIENCE
_(ask yourself: how does this compare to govt?)_

What is Resilience... how is relation ?

- Ahau: what is it
  - theory of change
  - application
- Ahau: what's different about it, why?
  - others holding our whakapapa is not appropriate
  - others holding keys to our whakapapa is not appropriate
    - licensing / subscription?
    - privacy

  - FOSS archiving exists ... but run own data services is fragile, requires
    - resiliant infrastructure
    - resiliant expertise
  - replicating data (distributed ledgers)
    - not backups, this requires too much expertise
    - how about everything is replicated?

- Scuttlebutt
  - Signed content (cryptographiclly verify who content came from)
  - Content Addressable Storage (hashes of content provide unique, immutable IDs)
  - Conflict-free Replicatable Data Types (content carries CRDTs operations, allowing deterministic resolution of state amongst peers)
  - Directed Acyclic Graphs (no shared "clock", so build causal "tangles" by backlinking to earlier operations)
  - Set Reconcilliation (for replication)
  - Encryption (trusted peers can help convey, but not read content)

- What data flow looks like
  - viz
  - where Ahau is in use
    - NZ (which groups can we name?)
    - Brazil (EDT)
    - review user data, collaborations?
  - doesn't need internet!


### PART 2: PLURALISM

when sovereignty and collaboration come into contact you either need to submit to one
sovergn state, or commit to figuring out how to communicate

many stories coming together, not just one story 

- many writers
  - many sources of truth
  - many types of truth can be contributed
    - beyond "one size fits all"

- truth is subjective
  - it always was
  - "free listening"
    - social graphs
    - attestation
  - "gatekeeping" looks different

- examples:
  - place names
  - different ways to display the same family tree
  - submissions vs kaitiaki verification

- data as collaboration

### PART 3: BEYOND SOVEREIGNTY

- soveriegn infrastructure
  - physical resilience (disaster resilience)
  - political resilience (not vulnerable to other states)
  - flexible (future resilience)
  - affordable (money resilience)


- who authors identity?
  - how do we access:
    - bank accounts
    - university
  












