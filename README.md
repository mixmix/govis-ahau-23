# ĀHAU - Distributed Replicating Tribal Archives

_GOVIS 23 talk_

## Précis / Summary

Āhau is a whakapapa project which is Māori-led. We've built software on a
distributed, replicating database, which offers some novel affordances:
- resilient infrastructure - no special computers/servers to maintain, no single
  points of failure
- offline first - each member of a whānau caries their data, meaning they can
  read it wherever they are, independent of current connectivity
- data sovereignty - knowledge is held only between those it belongs to
- emergent truth(s) - with no single source of truth (one database), we build
  truth together, supporting pluralism

Lessons for government:
- shared data sovereignty is possible!
- we can do resilience without without multi-nationals <3


## Bio

Mix has a background in Maths, Radical Education, Social Enterprise, Coops,
Software, and Community Gardening (tending to, and cultivating community). Āhau
has been his main focus for the past 4+ years, but works on a lot of projects:
- Commons Garden - governance systems for healthier social networks in the
  p2p/fedi space (where there are no bosses)
- Scuttlebutt - working on a European grant: Moderation in P2P Encrypted Groups
- SMAT - building visualisation tools to help fight fascism in social media
- Planetary - technical advising, community bridge
- Parenting - I have two gorgeous kids

Other interests include: Sci-fi, Magic the Gathering, Peer support networks,
Reforming education, Open banking APIs (it's *our* data)


## Dev / present

```bash
$ npm i -g docsify-cli
$ docsify serve docs
```

Use [←] and [→] keys to navigate between slides

## Deploy

- push master branch to gitlab
- go to : https://mixmix.gitlab.io/govis23-ahau
