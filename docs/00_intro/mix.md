<!-- file:00_intro/mix.md -->

![](te-mata-peak.jpg)

_Te Mata Peak, Tuki Tuki river_

- pakeha, tangata tiriti

<!-- slide:break-40 -->

![](sunset.jpg)

_High clouds at sunset over the Heretaunga plains._
