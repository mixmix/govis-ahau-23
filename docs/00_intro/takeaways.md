<!-- file:00_intro/takeaways.md -->

- resilience is beyond the corporate cloud
- data created between people should be held between people
- databases can be humanising

<style>

section.content article {
  height: 100%;
  max-width: 40vw;

  display: grid;
  justify-content: center;
  align-content: center;
  justify-items: center;

  grid-gap: 50px;
}

section.content li {
  font-size: 4vh; 
  line-height: 5vh;

  margin-bottom: 2vh;
}
</style>
