<!-- file:01_archiving/in_use/aotearoa.md -->

# Āhau in Aotearoa

![whangaroa whenua](whangaroa.jpg)

_Panoramoa of Whangaroa whenua (land)_

- partnerships with several hapū / iwi
- self-starters
  - e.g. community printing a whakapapa with 1300 people in it (20m x 1.5m)
