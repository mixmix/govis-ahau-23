<!-- file:01_archiving/mauri.md -->

# Mauri

> ...our traditional knowledge teaches us that all natural objects have a [wairua](https://maoridictionary.co.nz/search?keywords=wairua) and a [mauri](https://maoridictionary.co.nz/search?keywords=mauri). If our mauri is healthy then our wairua is intact and will make the natural object healthy. Moreover, if we touch something, share our thoughts or are involved with an event, then our mauri is left with that object or event.
>
> The same is also applicable to Māori data. Māori data contains mauri of the individual, whānau, hapū and Iwi. Therefore, any system that has Māori data then has peoples Mauri. If our data and mauri is being used for things that we are not aware of, stored overseas, or in culturally unsafe environments, then this will impact our wairua which will in turn make the individual, whānau, hapū and Iwi unhealthy.
>
> _— Dr Karaitiana Taiuru_

_source: https://www.taiuru.maori.nz/maori-data-sovereignty-rights-for-well-being_

<!-- slide:break-70 -->
