<!-- file:01_archiving/mauri_and_pakeha.md -->

![scientific american article](scientific_american.png)

src: https://www.scientificamerican.com/article/how-indigenous-groups-are-leading-the-way-on-data-privacy

<style>
img {
  box-shadow: 5px 5px 0px #444;
  border: 1px #444 solid;
  max-width: 754px;
}
</style>
