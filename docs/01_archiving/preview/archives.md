<!-- file:01_archiving/preview/archives.md -->

# Archives

![](archives.png)

_A story about some of Mix's ancestors, including a photo of them from the 1950s._

Supports archiving and connection of
- photos
- documents
- audio/ video
