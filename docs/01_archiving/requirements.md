<!-- file:01_archiving/requirements.md -->

# Requirements

- sovereignty
- culturally safe
- flexible
- maintainable
- accessible
- recoverable
