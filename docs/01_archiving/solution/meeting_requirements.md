<!-- file:01_archiving/solution/meeting_requirements.md -->

# Solution

Requirement     | Solution
----------------|---------------------------------------------------
sovereignty     | self hosted, open source
cultural safety | encryption as default, cultural policies
flexible        | (see later)
accessible      | run on any computer/ phone, works offline
maintaintable   | software is FOSS, install on any device
recoverable     | all commnunity members "remember" (are backups)
