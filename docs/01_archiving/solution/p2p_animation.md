<!-- file:01_archiving/solution/p2p_animation.md -->

# P2P

![p2p animation](multiple-pātaka_css.svg)

_Animation showing the distributed / peer-to-peer nature of Āhau._

<style>
img {
  max-height: 80vh;
}
</style>
