<!-- file:02_pluralism/implications/collaborations.md -->

# Records are collaborations

- many sources of truth (no single author)
- verification by kaitiaki/ historians
- conflicting opinions are expected and accounted for
- truth is emergent

<!-- TODO CRDT editing true image -->
