<!-- file:02_pluralism/implications/data_between.md -->

# Data lives between us

- no physical "home"
- servers "help", but are never "The Server"
- no gatekeepers to accessing your date (unlike APIs)

![](../../01_archiving/solution/multiple-pātaka_css.svg)

> AWS is not resilient <br />
> APIs are not resilient (see reddit)

<style>
img {
  max-height: 40vh;
}
</style>
