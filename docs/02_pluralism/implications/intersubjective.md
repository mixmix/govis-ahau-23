<!-- file:02_pluralism/implications/intersubjective.md -->

# Intersubjective truth

Examples:
- multiple versions of a record
- kaitiaki-only aspect to records
- whāngai links

![](whangai.svg)

_Whakapapa tree showing whāngai relationship (dotted line), and a birth relationship (blue line)._

<style>
img {
  max-height: 50vh;
  margin-top: 2vh;
}
</style>