<!-- file:02_pluralism/implications/truths_whakapapa.md -->

# Truths have their own whakapapa

- CRDT built from causally ordered, signed operations

```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)
E(E)

E-.->C-.->B-.->A
E-.->D-.->B
```
