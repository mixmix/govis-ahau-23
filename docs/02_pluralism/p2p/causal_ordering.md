<!-- file:02_pluralism/p2p/causal_ordering.md -->

# How do we order things without a shared clock time?

## ⇒ Causal Ordering

- no shared "clock" to order messages by
- build causal graphs based on links to prior messages
   - hash IDs mean you can only link backwards in time
   - DAGs - _Directed Acyclyic Graphs_

<!-- tabs:start -->
#### **simple**
```mermaid
graph BT
A(A):::root
B(B)
C(C)

C-.->B-.->A
```

#### **concurrent**
```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)

C-.->B-.->A
D-.->B
```

#### **merge**
```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)
E(E)

E-.->C-.->B-.->A
E-.->D-.->B
```
<!-- tabs:end -->

[](../../_p2p_style.md ':include')
