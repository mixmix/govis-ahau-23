<!-- file:02_pluralism/p2p/content_addressable.md -->

# How do you address something which lives in many places?

## ⇒ Content Addressable Storage

- the ID of a message is it's hash
- unique, immutable
- cannot be guessed ahead of time

![](hashing.png)

[](../../_p2p_style.md ':include')
