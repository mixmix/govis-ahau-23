<!-- file:02_pluralism/p2p/crdts.md -->

# How do you handle concurrent edits?

## ⇒ CRDTs

Conflict-free Replicated Data Types

- messages carry operations which mutate records
- careful design means deterministic resolution of state amongst peers

[](../../_p2p_style.md ':include')
