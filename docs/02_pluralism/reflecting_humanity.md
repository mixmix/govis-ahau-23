<!-- file:02_pluralism/reflecting_humanity.md -->

# Reflecting humanity

- Behaves *very* differently than corporate cloud
- Things which are singular are brittle (like a monoculture). 
- Beauracracies prefer legibility and simplicity, risk dehumanising.
