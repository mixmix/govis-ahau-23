<!-- file:03_sovereignty/institutionally_recognised_identity.md -->

# Institutionally recognised identity

![passport photo](passport.jpg)

_Photo of a Mix's passport (a verifiable credential issued by DIA)_

- In a country founded on Te Tiriti, shouldn't an Iwi be able to issue identity credentials?
- Āhau is working on this

<style>
img { max-height: 40vh; }
</style>
