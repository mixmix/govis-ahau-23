<!-- file:03_sovereignty/resilient_process.md -->

# Resilient process

- sovereignty (in the right places)
- culturally safe
- flexible
- maintainable
- accessible
- recoverable
