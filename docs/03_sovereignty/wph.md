<!-- file:03_sovereignty/wph.md -->

# Whangaroa Papa Hapū

<!-- https://whangaroapapahapu.org.nz/gallery/# -->

```mermaid
flowchart LR

subgraph Iwi[" "]
  direction BT

  PapaHapu([Papa Hapū])
  Hapu1([Hapū])
  Hapu2([Hapū])
  Whanau1A([Whānau])
  Whanau1B([Whānau])
  Whanau2A([Whānau])
  Whanau2B([Whānau])
  Whanau2C([Whānau])

  Whanau1A-->Hapu1-->PapaHapu
  Whanau1B-->Hapu1
  Whanau2A-->Hapu2-->PapaHapu
  Whanau2B-->Hapu2
  Whanau2C-->Hapu2
end

Iwi-."treaty settlement".-Crown

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;

classDef crownClass fill: #4c44cf, stroke-width: 0, color: white;
class Crown crownClass;
```

- treaty settlement
- crown requires a tribal registry
- Papa hapū acknowldge and respect Hapū rangatiratanga - their own autonomy
