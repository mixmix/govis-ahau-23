![logo](./assets/logo_red.svg)

# Āhau

## [Distributed Replicating Tribal Archives](00_intro/mix.md)


<p>
  <a href="#/00_intro/mix">
    Click to Start
  </a>
  then navigate with arrows &nbsp; <code>←</code> <code>→</code> 
</p>

<!-- background color -->

<!-- ![color](#f0f0f0) -->

<!-- background image -->

![](./assets/niho.svg)
