<style> 
.markdown-section > h2 span {
  color: var(--theme-color);
}

.markdown-section > ul {
  margin-left: 20px;
}
.markdown-section > p {
  margin-left: 30px;
}

.docsify-tabs {
  margin-left: 20px;
}
</style>
