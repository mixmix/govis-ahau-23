<!-- _title_style.md is a partial to import -->

<style>
section.content article {
  height: 100%;

  display: grid;
  justify-content: center;
  align-content: center;
  justify-items: center;

  grid-gap: 50px;
}

.markdown-section > h1 {
  font-size: 20vh;
  line-height: 0.8em;
  margin: 0;
}
.markdown-section > h2 {
  font-size: 10vh;
  line-height: 0.8em;
  margin: 0;

  margin-bottom: 16vh
}

.markdown-section > p {
  font-size: 3vh;
  margin: 0;
}
</style>
