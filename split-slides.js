const { readFileSync, mkdirSync, writeFileSync } = require('fs')
const { join, dirname } = require('path')

// building sidebar?
//   - sidebar may not be needed (auto-generate?)

const srcText = readFileSync(join(__dirname, 'src.md'), 'utf8')

const slides = srcText.split(/\n---\n/)
const fileNames = slides
  .map(slide => {
    const match = slide.match(/<!--\s?file:([^\s]+)\s?-->/)
    return match && match[1]
  })
  .filter(Boolean)

// checks for duplicate or missing names
if (slides.length !== new Set(fileNames).size) {
  console.log({
    slidesLength: slides.length,
    fileNamesLength: fileNames.length,
    fileNames
  })
  throw new Error('missing or duplicate filenames')
}

slides.forEach((slide, i) => {
  const filePath = join(__dirname, 'docs', fileNames[i])
  const fileDir = dirname(filePath)

  mkdirSync(fileDir, { recursive: true })
  writeFileSync(filePath, slide)
  console.log('✓', fileNames[i])
})
