<!-- file:00_intro/mix.md -->

![](te-mata-peak.jpg)

_Te Mata Peak, Tuki Tuki river_

- pakeha, tangata tiriti

<!-- slide:break-40 -->

![](sunset.jpg)

_High clouds at sunset over the Heretaunga plains._

---
<!-- file:00_intro/takeaways.md -->

- resilience is beyond the corporate cloud
- data created between people should be held between people
- databases can be humanising

<style>

section.content article {
  height: 100%;
  max-width: 40vw;

  display: grid;
  justify-content: center;
  align-content: center;
  justify-items: center;

  grid-gap: 50px;
}

section.content li {
  font-size: 4vh; 
  line-height: 5vh;

  margin-bottom: 2vh;
}
</style>

---
<!-- file:01_archiving/README.md -->

# Part 1
## Tribal Archiving

[](../_title_style.md ':include')

---
<!-- file:01_archiving/origins.md -->

# Āhau's origins

![](origin.png)

- Maori led organisation
- Build tools which support community coordination

---
<!-- file:01_archiving/preview/whakapapa.md -->

# Whakapapa

![](irving_family.png)

_A screenshot of some of Mix's whakapapa (family tree)_


Āhau is a desktop + phone application for whakapapa

---
<!-- file:01_archiving/preview/archives.md -->

# Archives

![](archives.png)

_A story about some of Mix's ancestors, including a photo of them from the 1950s._

Supports archiving and connection of
- photos
- documents
- audio/ video

---
<!-- file:01_archiving/preview/registry.md -->

# Tribal Registry

![](registry.png)

_A registry of members of a whānau / hapū / Iwi._

---
<!-- file:01_archiving/requirements.md -->

# Requirements

- sovereignty
- culturally safe
- flexible
- maintainable
- accessible
- recoverable

---
<!-- file:01_archiving/mauri.md -->

# Mauri

> ...our traditional knowledge teaches us that all natural objects have a [wairua](https://maoridictionary.co.nz/search?keywords=wairua) and a [mauri](https://maoridictionary.co.nz/search?keywords=mauri). If our mauri is healthy then our wairua is intact and will make the natural object healthy. Moreover, if we touch something, share our thoughts or are involved with an event, then our mauri is left with that object or event.
>
> The same is also applicable to Māori data. Māori data contains mauri of the individual, whānau, hapū and Iwi. Therefore, any system that has Māori data then has peoples Mauri. If our data and mauri is being used for things that we are not aware of, stored overseas, or in culturally unsafe environments, then this will impact our wairua which will in turn make the individual, whānau, hapū and Iwi unhealthy.
>
> _— Dr Karaitiana Taiuru_

_source: https://www.taiuru.maori.nz/maori-data-sovereignty-rights-for-well-being_

<!-- slide:break-70 -->

---
<!-- file:01_archiving/mauri_and_pakeha.md -->

![scientific american article](scientific_american.png)

src: https://www.scientificamerican.com/article/how-indigenous-groups-are-leading-the-way-on-data-privacy

<style>
img {
  box-shadow: 5px 5px 0px #444;
  border: 1px #444 solid;
  max-width: 754px;
}
</style>

---
<!-- file:01_archiving/solution/p2p_animation.md -->

# P2P

![p2p animation](multiple-pātaka_css.svg)

_Animation showing the distributed / peer-to-peer nature of Āhau._

<style>
img {
  max-height: 80vh;
}
</style>

---
<!-- file:01_archiving/solution/meeting_requirements.md -->

# Solution

Requirement     | Solution
----------------|---------------------------------------------------
sovereignty     | self hosted, open source
cultural safety | encryption as default, cultural policies
flexible        | (see later)
accessible      | run on any computer/ phone, works offline
maintaintable   | software is FOSS, install on any device
recoverable     | all commnunity members "remember" (are backups)

---
<!-- file:01_archiving/in_use/aotearoa.md -->

# Āhau in Aotearoa

![whangaroa whenua](whangaroa.jpg)

_Panoramoa of Whangaroa whenua (land)_

- partnerships with several hapū / iwi
- self-starters
  - e.g. community printing a whakapapa with 1300 people in it (20m x 1.5m)

---
<!-- file:01_archiving/in_use/earth_defenders.md -->

# Āhau in the Americas

![eath defenders](edt.png)

_Recent Earth Defenders gathering in Tena, Ecuador_

- Ecuador
- Brazil
- Guyana
- Canada

---
<!-- file:01_archiving/in_use/pakeha.md -->

# Āhau for Pākeha

![Translating Āhau](american_english.png)

_Image showing translation of Āhau from NZ English ⇒ American English_

---
<!-- file:02_pluralism/README.md -->

# Part 2
## Pluralism

OR

How having no single source of truth is human

[](../_title_style.md ':include')

---
<!-- file:02_pluralism/p2p/content_addressable.md -->

# How do you address something which lives in many places?

## ⇒ Content Addressable Storage

- the ID of a message is it's hash
- unique, immutable
- cannot be guessed ahead of time

![](hashing.png)

[](../../_p2p_style.md ':include')

---
<!-- file:02_pluralism/p2p/signed_messages.md -->

# How can you trust "gossip"?

## ⇒ Signed messages

- cryptographiclly verify who authored a message

[](../../_p2p_style.md ':include')

---
<!-- file:02_pluralism/p2p/crdts.md -->

# How do you handle concurrent edits?

## ⇒ CRDTs

Conflict-free Replicated Data Types

- messages carry operations which mutate records
- careful design means deterministic resolution of state amongst peers

[](../../_p2p_style.md ':include')

---
<!-- file:02_pluralism/p2p/causal_ordering.md -->

# How do we order things without a shared clock time?

## ⇒ Causal Ordering

- no shared "clock" to order messages by
- build causal graphs based on links to prior messages
   - hash IDs mean you can only link backwards in time
   - DAGs - _Directed Acyclyic Graphs_

<!-- tabs:start -->
#### **simple**
```mermaid
graph BT
A(A):::root
B(B)
C(C)

C-.->B-.->A
```

#### **concurrent**
```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)

C-.->B-.->A
D-.->B
```

#### **merge**
```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)
E(E)

E-.->C-.->B-.->A
E-.->D-.->B
```
<!-- tabs:end -->

[](../../_p2p_style.md ':include')

---
<!-- file:02_pluralism/implications/data_between.md -->

# Data lives between us

- no physical "home"
- servers "help", but are never "The Server"
- no gatekeepers to accessing your date (unlike APIs)

![](../../01_archiving/solution/multiple-pātaka_css.svg)

> AWS is not resilient <br />
> APIs are not resilient (see reddit)

<style>
img {
  max-height: 40vh;
}
</style>

---
<!-- file:02_pluralism/implications/collaborations.md -->

# Records are collaborations

- many sources of truth (no single author)
- verification by kaitiaki/ historians
- conflicting opinions are expected and accounted for
- truth is emergent

<!-- TODO CRDT editing true image -->

---
<!-- file:02_pluralism/implications/intersubjective.md -->

# Intersubjective truth

Examples:
- multiple versions of a record
- kaitiaki-only aspect to records
- whāngai links

![](whangai.svg)

_Whakapapa tree showing whāngai relationship (dotted line), and a birth relationship (blue line)._

<style>
img {
  max-height: 50vh;
  margin-top: 2vh;
}
</style>
---
<!-- file:02_pluralism/implications/truths_whakapapa.md -->

# Truths have their own whakapapa

- CRDT built from causally ordered, signed operations

```mermaid
graph BT
A(A):::root
B(B)
C(C)
D(D)
E(E)

E-.->C-.->B-.->A
E-.->D-.->B
```

---
<!-- file:02_pluralism/reflecting_humanity.md -->

# Reflecting humanity

- Behaves *very* differently than corporate cloud
- Things which are singular are brittle (like a monoculture). 
- Beauracracies prefer legibility and simplicity, risk dehumanising.

---
<!-- file:03_sovereignty/README.md -->

# Part 3
## Sovereignty

[](../_title_style.md ':include')

---
<!-- file:03_sovereignty/wph.md -->

# Whangaroa Papa Hapū

<!-- https://whangaroapapahapu.org.nz/gallery/# -->

```mermaid
flowchart LR

subgraph Iwi[" "]
  direction BT

  PapaHapu([Papa Hapū])
  Hapu1([Hapū])
  Hapu2([Hapū])
  Whanau1A([Whānau])
  Whanau1B([Whānau])
  Whanau2A([Whānau])
  Whanau2B([Whānau])
  Whanau2C([Whānau])

  Whanau1A-->Hapu1-->PapaHapu
  Whanau1B-->Hapu1
  Whanau2A-->Hapu2-->PapaHapu
  Whanau2B-->Hapu2
  Whanau2C-->Hapu2
end

Iwi-."treaty settlement".-Crown

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #fff, color: #240e5e, stroke: #240e5e, stroke-width: 1;

classDef crownClass fill: #4c44cf, stroke-width: 0, color: white;
class Crown crownClass;
```

- treaty settlement
- crown requires a tribal registry
- Papa hapū acknowldge and respect Hapū rangatiratanga - their own autonomy

---
<!-- file:03_sovereignty/resilient_process.md -->

# Resilient process

- sovereignty (in the right places)
- culturally safe
- flexible
- maintainable
- accessible
- recoverable

---
<!-- file:03_sovereignty/institutionally_recognised_identity.md -->

# Institutionally recognised identity

![passport photo](passport.jpg)

_Photo of a Mix's passport (a verifiable credential issued by DIA)_

- In a country founded on Te Tiriti, shouldn't an Iwi be able to issue identity credentials?
- Āhau is working on this

<style>
img { max-height: 40vh; }
</style>

---
<!-- file:03_sovereignty/further.md -->

# Further

Where else is data that's connected to me being held?
- banking
- health
  - doctors
  - hospitals
  - dentists
- education
- realme

---
<!-- file:03_sovereignty/takeaways.md -->

- resilience is beyond the corporate cloud
- data created between people should be held between people
- databases can be humanising

<style>

section.content article {
  height: 100%;
  max-width: 40vw;

  display: grid;
  justify-content: center;
  align-content: center;
  justify-items: center;

  grid-gap: 50px;
}

section.content li {
  font-size: 4vh; 
  line-height: 5vh;

  margin-bottom: 2vh;
}
</style>

---
<!-- file:03_sovereignty/final.md -->

![Āhau logo](../assets/logo_red.svg)

## [ahau.io](https://ahau.io)

###### [chat.ahau.io](http://chat.ahau.io) / [mix@protozoa.nz](mailto:mix@protozoa.nz)

<style>
img {
  height: 40vh;
  margin-top: 10vh;
}

.markdown-section > h2 {
  margin-bottom: 0 !important;
}

a {
  text-decoration: none;
}

</style>
[](../_title_style.md ':include')
